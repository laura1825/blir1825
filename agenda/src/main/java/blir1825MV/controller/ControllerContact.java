package blir1825MV.controller;

import blir1825MV.model.base.Contact;
import blir1825MV.model.repository.interfaces.RepositoryContact;

import java.util.List;

public class ControllerContact {
    private RepositoryContact repo;


    public ControllerContact(RepositoryContact repo) {
        this.repo = repo;
    }

    public void addContract(Contact c) {
        repo.addContact(c);
    }

    public int getCount() {
        return repo.count();
    }

    public Contact getByName(String name) {
        return repo.getByName(name);
    }

    public List<Contact> getContacts() {
        return repo.getContacts();
    }
}
