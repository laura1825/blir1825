package blir1825MV.controller;

import blir1825MV.model.base.Activity;
import blir1825MV.model.repository.interfaces.RepositoryActivity;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ControllerActivity {
    private RepositoryActivity repo;

    public ControllerActivity(RepositoryActivity repo) {
        this.repo = repo;
    }

    public boolean addActivity(Activity activity) {
        return repo.addActivity(activity);
    }

    public List<Activity> activitiesOnDate(String name, Date d) {
        return repo.activitiesOnDate(name, d);
    }

    public List<Activity> orderByStartTime(List<Activity> activities) {
        //activities.sort();

        activities.sort(new Comparator<Activity>() {
            @Override
            public int compare(Activity o1, Activity o2) {
                return o1.getStart().compareTo(o2.getStart());
            }
        });
        return activities;
    }
}
