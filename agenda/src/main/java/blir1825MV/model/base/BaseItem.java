package blir1825MV.model.base;

public class BaseItem {
    protected String name;

    public BaseItem(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
