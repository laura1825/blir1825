package blir1825MV.model.base;

import blir1825MV.exceptions.InvalidFormatException;
import blir1825MV.model.validator.ValidatorContact;

public class Contact {
	private String name;
	private String address;
	private String telefon;
	private String email;
	private static ValidatorContact validator = new ValidatorContact();
	
	public Contact(){
		name = "";
		address = "";
		telefon = "";
		email="";
	}
	
	public Contact(String name, String address, String telefon, String email) throws InvalidFormatException{
		validator.validate(name, address, telefon, email);
		this.name = name;
		this.address = address;
		this.telefon = telefon;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws InvalidFormatException {
		if (!validator.validName(name)) throw new InvalidFormatException("Cannot convert", "Invalid name");
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) throws InvalidFormatException {
		if (!validator.validAddress(address)) {
			throw new InvalidFormatException("Cannot convert", "Invalid address");
		}
		this.address = address;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) throws InvalidFormatException {
		if (!validator.validTelefon(telefon)) {
			throw new InvalidFormatException("Cannot convert Invalid phone number", "Invalid phone number");
		}
		this.telefon = telefon;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) throws InvalidFormatException {
		if (!validator.validEmail(email)) throw new InvalidFormatException("Cannot convert Invalid email", "Invalid email");
		this.email = email;
	}

	public static Contact fromString(String str, String delim) throws InvalidFormatException
	{
		String[] s = str.split(delim);
		if (s.length != 4) throw new InvalidFormatException("Cannot convert", "Invalid data");
		validator.validate(s[0], s[1], s[2], s[3]);

		return new Contact(s[0], s[1], s[2], s[3]);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("#");
		sb.append(address);
		sb.append("#");
		sb.append(telefon);
		sb.append("#");
		sb.append(email);
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Contact)) return false;
		Contact o = (Contact)obj;
		if (name.equals(o.name) && address.equals(o.address) && telefon.equals(o.telefon) && email.equals(o.email))
			return true;
		return false;
	}
}
