package blir1825MV.model.base;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import blir1825MV.model.repository.interfaces.RepositoryContact;

public class Activity {
	private String userName;
	private String name;
	private String description;
	private String place;
	private Date start;
	private Date end;
	private List<Contact> contacts;

	
	public Activity(String userName, String name, String description, String place, Date start, Date end, List<Contact> contacts) {
		this.userName = userName;
		this.name = name;
		this.description = description;
		this.place = place;
		if (contacts == null)
			this.contacts = new LinkedList<Contact>();
		else
			this.contacts = new LinkedList<Contact>(contacts);

		this.start = new Date();
		this.start.setTime(start.getTime());
		this.end = new Date();
		this.end.setTime(end.getTime());
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Activity))
			return false;
		Activity act = (Activity) obj;
		if (act.description.equals(description) && start.equals(act.start)
				&& end.equals(act.end) && name.equals(act.name))
			return true;
		return false;
	}

	public boolean intersect(Activity act) {
		if (start.compareTo(act.end) < 0
				&& act.start.compareTo(end) < 0)
			return true;
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(userName);
		sb.append("#");
		sb.append(name);
		sb.append("#");
		sb.append(description);
		sb.append("#");
		sb.append(place);
		sb.append("#");
		sb.append(start.getTime());
		sb.append("#");
		sb.append(end.getTime());
		for (Contact c : contacts) {
			sb.append("#");
			sb.append(c.getName());
		}
		return sb.toString();
	}

	public static Activity fromString(String line, RepositoryContact repcon) {
		try {
			String[] str = line.split("#");
			String userName = str[0];
			String name = str[1];
			String description = str[2];
			String place = str[3];
			Date start = new Date(Long.parseLong(str[4]));
			Date end = new Date(Long.parseLong(str[5]));

            List<Contact> conts = new LinkedList<Contact>();
            if (str.length != 5) {
                for (int i = 6; i < str.length; i++) {
                    conts.add(repcon.getByName(str[i]));
                }
            }

			return new Activity(userName, name, description, place, start, end, conts);
		} catch (Exception e) {
			return null;
		}
	}

//	public int compareTo(Activity a) {
//	    return start.compareTo(a.getStart());
//    }

}
