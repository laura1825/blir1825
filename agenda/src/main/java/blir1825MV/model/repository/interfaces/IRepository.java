package blir1825MV.model.repository.interfaces;

import blir1825MV.model.base.BaseItem;
import java.util.List;

public interface IRepository {
    List<BaseItem> getItems();
    boolean add(BaseItem item);
    boolean remove(BaseItem item);
    boolean saveToFile();
    int count();
}
