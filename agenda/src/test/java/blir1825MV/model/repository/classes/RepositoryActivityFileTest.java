package blir1825MV.model.repository.classes;

import blir1825MV.Utils.Utils;
import blir1825MV.model.base.Activity;
import blir1825MV.model.repository.interfaces.RepositoryContact;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RepositoryActivityFileTest {
    RepositoryContact _contactRepo;
    RepositoryActivityFile _repo;

    public RepositoryActivityFileTest() throws Exception {
        _contactRepo = new RepositoryContactFile();
        _repo = new RepositoryActivityFile(_contactRepo);
    }

    @Test
    public void addActivity() throws Exception {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity act = new Activity("username1", "act1", "fun", "Madrid", df.parse("04/20/2013 12:00"),
                        df.parse("04/25/2013 14:00"), Utils.generateRandomContacts());
        boolean result = _repo.addActivity(act);

        assertEquals(result, true);
    }

    @Test
    public void addActivity1() throws Exception {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity act = new Activity("username1", "act1", "fun", "Madrid", df.parse("05/22/2013 12:00"),
                df.parse("05/28/2013 14:00"), Utils.generateRandomContacts());

        Activity act1 = new Activity("username1", "act1", "fun", "Madrid", df.parse("05/23/2013 12:00"),
                df.parse("05/25/2013 14:00"), Utils.generateRandomContacts());
        _repo.addActivity(act);
        boolean result = _repo.addActivity(act1);
        assertEquals(result, false);
    }

    @Test
    public void activityOnDate1() throws Exception {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity act = new Activity("username1", "act1", "fun", "Madrid", df.parse("05/27/2013 12:00"),
                df.parse("04/29/2013 14:00"), Utils.generateRandomContacts());
       _repo.addActivity(act);

       List<Activity> activities = _repo.activitiesOnDate("username1", df.parse("05/27/2013 12:00"));
       assertEquals(1, activities.size());
    }

    @Test
    public void activityOnDate2() throws Exception {
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");

        List<Activity> activities = _repo.activitiesOnDate("username1", df.parse("05/23/2013 12:00"));
        assertEquals(0, activities.size());
    }
}