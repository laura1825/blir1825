package blir1825MV.model.repository.classes;

import blir1825MV.model.base.Contact;
import blir1825MV.model.repository.interfaces.RepositoryContact;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RepositoryContactFileTest {
    RepositoryContact _contactRepo;

    public RepositoryContactFileTest() throws Exception {
        _contactRepo = new RepositoryContactFile();
    }

    @Test
    public void addContact() throws Exception {
        int len1 = _contactRepo.getContacts().size();
        Contact contact = new Contact("laura", "streiului1", "0234567891", "laura@yahoo.com");
        _contactRepo.addContact(contact);

        assertEquals(len1 + 1, _contactRepo.getContacts().size());
    }
}
