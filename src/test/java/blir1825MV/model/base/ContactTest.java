package blir1825MV.model.base;

import blir1825MV.exceptions.InvalidFormatException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class ContactTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void setTelefon1() throws Exception {
        // arrange
        Contact contact = new Contact();

        // act
        contact.setTelefon("0264578691");

        // assert
        assertEquals("0264578691", contact.getTelefon());
    }

    @Test
    public void setTelefon2() throws Exception {
        // arrange
        Contact contact = new Contact();

        // act assert
        thrown.expect(InvalidFormatException.class);
        thrown.expectMessage("Cannot convert Invalid phone number");
        contact.setTelefon("+04 762 789 387");
    }

    @Test
    public void setTelefon3() throws Exception {
        // arrange
        Contact contact = new Contact();

        // act assert
        thrown.expect(InvalidFormatException.class);
        thrown.expectMessage("Cannot convert Invalid phone number");
        contact.setTelefon("023456789");
    }

    @Test
    public void setTelefon4() throws Exception {
        // arrange
        Contact contact = new Contact();

        // act assert
        thrown.expect(InvalidFormatException.class);
        thrown.expectMessage("Cannot convert Invalid phone number");
        contact.setTelefon("02345678912");
    }

    @Test
    public void setTelefon5() throws Exception {
        // arrange
        Contact contact = new Contact();

        // act
        contact.setTelefon("+4078989807");

        // assert
        assertEquals("+4078989807", contact.getTelefon());
    }

    @Test
    public void setTelefon6() throws Exception {
        // arrange
        Contact contact = new Contact();

        // act assert
        thrown.expect(InvalidFormatException.class);
        thrown.expectMessage("Cannot convert Invalid phone number");
        contact.setTelefon("9123456789");
    }

    @Test
    public void setTelefon7() throws Exception {
        // arrange
        Contact contact = new Contact();

        // act assert
        thrown.expect(InvalidFormatException.class);
        thrown.expectMessage("Cannot convert Invalid phone number");
        contact.setTelefon("+04 762 789 387");
    }

    @Test
    public void setEmail8() throws Exception {
        // arrange
        Contact contact = new Contact();

        // act
        contact.setEmail("laura@gmail.com");

        // assert
        assertEquals("laura@gmail.com", contact.getEmail());
    }

    @Test
    public void setEmail9() throws Exception {
        // arrange
        Contact contact = new Contact();

        // act assert
        thrown.expect(InvalidFormatException.class);
        thrown.expectMessage("Cannot convert Invalid email");
        contact.setEmail("laura@.com");
    }

    @Test
    public void setEmail10() throws Exception {
        // arrange
        Contact contact = new Contact();

        // act assert
        thrown.expect(InvalidFormatException.class);
        thrown.expectMessage("Cannot convert Invalid email");
        contact.setEmail("laura@yahooo.");
    }

}