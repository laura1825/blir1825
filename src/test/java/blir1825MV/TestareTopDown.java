package blir1825MV;

import blir1825MV.Utils.Utils;
import blir1825MV.model.base.Activity;
import blir1825MV.model.base.Contact;
import blir1825MV.model.repository.classes.RepositoryActivityFile;
import blir1825MV.model.repository.classes.RepositoryContactFile;
import blir1825MV.model.repository.interfaces.RepositoryContact;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestareTopDown {
    RepositoryContact _contactRepo;
    RepositoryActivityFile _repo;

    public TestareTopDown() throws Exception {
        _contactRepo = new RepositoryContactFile();
        _repo = new RepositoryActivityFile(_contactRepo);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void addContact() throws Exception {
        int len1 = _contactRepo.getContacts().size();
        Contact contact = new Contact("laura", "streiului1", "0234567891", "laura@yahoo.com");
        _contactRepo.addContact(contact);

        assertEquals(len1 + 1, _contactRepo.getContacts().size());
    }

    @Test
    public void integrareB() throws Exception {
        addContact();

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity act = new Activity("username1", "act1", "fun", "Madrid", df.parse("09/20/2013 12:00"),
                df.parse("09/25/2013 14:00"), Utils.generateRandomContacts());
        boolean result = _repo.addActivity(act);

        assertEquals(result, true);
    }

    @Test
    public void integrareC() throws Exception {
        addContact();

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity act = new Activity("username1", "act1", "fun", "Madrid", df.parse("10/20/2013 12:00"),
                df.parse("10/25/2013 14:00"), Utils.generateRandomContacts());
        boolean result = _repo.addActivity(act);

        assertEquals(result, true);

        List<Activity> activities = _repo.activitiesOnDate("username1", df.parse("10/20/2013 12:00"));
        assertEquals(1, activities.size());
    }

}
