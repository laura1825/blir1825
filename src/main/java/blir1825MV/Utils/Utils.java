package blir1825MV.Utils;

import blir1825MV.controller.ControllerContact;
import blir1825MV.model.base.Contact;
import blir1825MV.model.repository.classes.RepositoryActivityFile;
import blir1825MV.model.repository.classes.RepositoryContactFile;
import blir1825MV.model.repository.interfaces.RepositoryContact;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class Utils {
//    RepositoryContact _contactRepo;
//    RepositoryActivityFile _repo;
//    private static ControllerContact _contractCtrl;
//
//    public Utils() throws Exception {
//        _contactRepo = new RepositoryContactFile();
//        _repo = new RepositoryActivityFile(_contactRepo);
//        _contractCtrl = new ControllerContact(_contactRepo);
//    }

    public static List<Contact> generateRandomContacts() throws Exception {
        RepositoryContact _contactRepo = new RepositoryContactFile();
        RepositoryActivityFile _repo = new RepositoryActivityFile(_contactRepo);
        ControllerContact _contractCtrl = new ControllerContact(_contactRepo);

        List<Contact> contacts = new ArrayList<Contact>();
        Random rand = new Random();
        int max = _contractCtrl.getCount();
        int min = 0;
        int randomNumber = rand.nextInt( max- min + 1) + min;
        List<Contact> existedContacts = _contractCtrl.getContacts();

        for(int i = 0; i < randomNumber; i++) {
            randomNumber = rand.nextInt( max- min) + min;
            Contact c = existedContacts.get(randomNumber);
            contacts.add(c);
        }

        return contacts;
    }
}
