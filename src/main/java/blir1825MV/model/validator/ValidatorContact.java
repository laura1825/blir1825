package blir1825MV.model.validator;

import blir1825MV.exceptions.InvalidFormatException;
import blir1825MV.model.base.Contact;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidatorContact {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public boolean validate(String name, String address, String telefon, String email) throws InvalidFormatException {
        if (!validName(name)) throw new InvalidFormatException("Cannot convert", "Invalid name");
        if (!validAddress(address)) throw new InvalidFormatException("Cannot convert", "Invalid address");
        if (!validTelefon(telefon)) throw new InvalidFormatException("Cannot convert", "Invalid phone number");
        if (!validEmail(email)) throw new InvalidFormatException("Cannot convert", "Invalid email");

        return true;
    }

    public static boolean validName(String str)
    {
        // Check if contains a punctuation character or a space
        String[] s = str.split("[\\p{Punct}\\s]+");
        if (str.length() == 0 || s.length >= 2) return false;
        return true;
    }

    public static boolean validAddress(String str) {
        if (str.length() == 0) return false;
        return true;
    }

    public static boolean validTelefon(String tel) {
        String[] s = tel.split("[\\p{Punct}\\s]+");
        if (tel.charAt(0) == '+' && s.length == 2 && tel.length() == 11 & tel.matches("^\\+\\d+")) return true; // +074567345
        if (tel.charAt(0) != '0' || !tel.matches("\\d+")) return false; // 0345789098
        if (tel.length() != 10) return false;
        return true;
    }

    public static boolean validEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        return matcher.find();
    }
}
