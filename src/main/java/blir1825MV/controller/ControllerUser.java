package blir1825MV.controller;

import blir1825MV.model.base.User;
import blir1825MV.model.repository.interfaces.RepositoryUser;

public class ControllerUser {
    private RepositoryUser repo;

    public ControllerUser(RepositoryUser repo) {
        this.repo = repo;
    }

    public User getByUsername(String userName) {
        return repo.getByUsername(userName);
    }
}
