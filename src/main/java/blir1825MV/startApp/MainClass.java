package blir1825MV.startApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import blir1825MV.controller.ControllerActivity;
import blir1825MV.controller.ControllerContact;
import blir1825MV.controller.ControllerUser;
import blir1825MV.exceptions.InvalidFormatException;
import blir1825MV.model.base.Activity;
import blir1825MV.model.base.Contact;
import blir1825MV.model.base.User;
import blir1825MV.model.repository.classes.RepositoryActivityFile;
import blir1825MV.model.repository.classes.RepositoryContactFile;
import blir1825MV.model.repository.classes.RepositoryUserFile;
import blir1825MV.model.repository.interfaces.RepositoryActivity;
import blir1825MV.model.repository.interfaces.RepositoryContact;
import blir1825MV.model.repository.interfaces.RepositoryUser;

//functionalitati
//i.	 adaugarea de contacte (nume, adresa, numar de telefon, adresa email);
//ii.	 programarea unor activitati (denumire, descriere, data, locul, ora inceput, durata, contacte).
//iii.	 generarea unui raport cu activitatile pe care le are utilizatorul (nume, user, parola) la o anumita data, ordonate dupa ora de inceput.

public class MainClass {

    private static ControllerContact contractCtrl;
    private static ControllerActivity activityCtrl;

	public static void main(String[] args) {
		BufferedReader in = null;
		try {
			RepositoryContact contactRep = new RepositoryContactFile();
			contractCtrl = new ControllerContact(contactRep);
			RepositoryUser userRep = new RepositoryUserFile();
            ControllerUser userCtrl = new ControllerUser(userRep);
			RepositoryActivity activityRep = new RepositoryActivityFile(contactRep);
			activityCtrl = new ControllerActivity(activityRep);

			User user = null;
			in = new BufferedReader(new InputStreamReader(System.in));
			while (user == null) {
				System.out.printf("Enter username: ");
				String u = in.readLine();
				System.out.printf("Enter password: ");
				String p = in.readLine();

				user = userCtrl.getByUsername(u);
				if (!user.isPassword(p))
					user = null;
			}

			int chosen = 0;
			while (chosen != 4) {
				printMenu();
				chosen = Integer.parseInt(in.readLine());
				try {
					switch (chosen) {
					case 1:
						adaugContact(contractCtrl, in);
						break;
					case 2:
						adaugActivitate(activityRep, contactRep, in, user);
						break;
					case 3:
						afisActivitate(activityRep, in, user);
						break;
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}

		} catch (Exception e) {
            System.out.println(e.getMessage());
		}
		System.out.println("Program over and out\n");
	}

	private static void afisActivitate(RepositoryActivity activityRep, BufferedReader in, User user) {
		try {
			System.out.printf("Afisare Activitate: \n");
			System.out.printf("Data(format: mm/dd/yyyy): ");
			String dateS = in.readLine();
			Calendar c = Calendar.getInstance();
			c.set(Integer.parseInt(dateS.split("/")[2]),
					Integer.parseInt(dateS.split("/")[0]) - 1,
					Integer.parseInt(dateS.split("/")[1]));
			Date d = c.getTime();

			System.out.println("Activitatile din ziua " + d.toString() + ": ");

			List<Activity> activities = activityCtrl.activitiesOnDate(user.getName(), d);
			List<Activity> sortedActivities = activityCtrl.orderByStartTime(activities);
			for (Activity a : sortedActivities) {
				System.out.printf("%s - %s: %s - %s with: ", a.getStart()
						.toString(), a.getEnd().toString(), a.getName(), a.getDescription());
				for (Contact con : a.getContacts())
					System.out.printf("%s, ", con.getName());
				System.out.println();
			}
		} catch (IOException e) {
			System.out.printf("Eroare de citire: %s\n" + e.getMessage());
		}
	}

	private static void adaugActivitate(RepositoryActivity activityRep, RepositoryContact contactRep, BufferedReader in, User user) {
		try {
			System.out.printf("Adauga Activitate: \n");
            System.out.printf("Denumire: ");
            String name = in.readLine();
			System.out.printf("Descriere: ");
			String description = in.readLine();
			System.out.printf("Start Date(format: mm/dd/yyyy): ");
			String dateS = in.readLine();
            System.out.printf("Loc: ");
            String place = in.readLine();
			System.out.printf("Start Time(hh:mm): ");
			String timeS = in.readLine();
			Calendar c = Calendar.getInstance();
			c.set(Integer.parseInt(dateS.split("/")[2]),
                    Integer.parseInt(dateS.split("/")[0]) - 1,
					Integer.parseInt(dateS.split("/")[1]),
					Integer.parseInt(timeS.split(":")[0]),
					Integer.parseInt(timeS.split(":")[1]));
			Date start = c.getTime();

			System.out.printf("End Date(format: mm/dd/yyyy): ");
			String dateE = in.readLine();
			System.out.printf("End Time(hh:mm): ");
			String timeE = in.readLine();
			
			c.set(Integer.parseInt(dateE.split("/")[2]),
					Integer.parseInt(dateE.split("/")[0]) - 1,
					Integer.parseInt(dateE.split("/")[1]),
					Integer.parseInt(timeE.split(":")[0]),
					Integer.parseInt(timeE.split(":")[1]));
			Date end = c.getTime();

			Activity act = new Activity(user.getName(), name, description, place, start, end, generateRandomContacts());

			activityCtrl.addActivity(act);

			System.out.printf("S-a adugat cu succes\n");
		} catch (IOException e) {
			System.out.printf("Eroare de citire: %s\n" + e.getMessage());
		}

	}

	private static List<Contact> generateRandomContacts() {
	    List<Contact> contacts = new ArrayList<Contact>();
        Random rand = new Random();
        int max = contractCtrl.getCount();
        int min = 0;
        int randomNumber = rand.nextInt( max- min + 1) + min;
        List<Contact> existedContacts = contractCtrl.getContacts();

	    for(int i = 0; i < randomNumber; i++) {
	        randomNumber = rand.nextInt( max- min + 1) + min;
	        Contact c = existedContacts.get(randomNumber);
            contacts.add(c);
        }

        return contacts;
    }

	private static void adaugContact(ControllerContact contactCtrl, BufferedReader in) {
		try {
			System.out.printf("Adauga Contact: \n");
			System.out.printf("Nume: ");
			String name = in.readLine();
			System.out.printf("Adresa: ");
			String adress = in.readLine();
			System.out.printf("Numar de telefon: ");
			String telefon = in.readLine();
			System.out.printf("Email: ");
			String email = in.readLine();
			
			Contact c = new Contact(name, adress, telefon, email);

            contactCtrl.addContract(c);

			System.out.printf("S-a adugat cu succes\n");
		} catch (IOException e) {
			System.out.printf("Eroare de citire: %s\n" + e.getMessage());
		} catch (InvalidFormatException e) {
			if (e.getCause() != null)
				System.out.printf("Eroare: %s - %s\n" + e.getMessage(), e
						.getCause().getMessage());
			else
				System.out.printf("Eroare: %s\n" + e.getMessage());
		}
	}

	private static void printMenu() {
		System.out.printf("Please choose option:\n");
		System.out.printf("1. Adauga contact\n");
		System.out.printf("2. Adauga activitate\n");
		System.out.printf("3. Afisare activitati din data de...\n");
		System.out.printf("4. Exit\n");
		System.out.printf("Alege: ");
	}
}
